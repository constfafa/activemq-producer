package cn.bellychang;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.jms.annotation.EnableJms;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class ActivemqProducerApplication {

	public static void main(String[] args) throws InterruptedException {
		SpringApplication.run(ActivemqProducerApplication.class, args);
	}
}
