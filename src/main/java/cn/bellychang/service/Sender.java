package cn.bellychang.service;

import cn.bellychang.model.Email;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import javax.jms.Queue;
import javax.jms.Topic;

/**
 * @author ChangLiang
 * @date 2018/5/29
 */
@Service
public class Sender {

    @Autowired
    private JmsTemplate jmsTemplate;

    @Autowired
    private Queue queue;

    @Autowired
    private Topic topic;

    int dots = 0;
    int count = 0;

    @Scheduled(fixedDelay = 1000, initialDelay = 500)
    public void send() {
        StringBuilder builder = new StringBuilder("Hello");
        if (dots++ == 3) {
            dots = 1;
        }
        int i = 0;
        for (; i < dots; i++) {
            builder.append('.');
        }
        builder.append(Integer.toString(++count));
        Email email = new Email("info@exmaple.com", builder.toString());
        if (i % 2 == 0) {
            jmsTemplate.convertAndSend(queue, email);
            System.out.println(" [x] Sent '" + email + email.toString() +"--queue");
        }else{
            jmsTemplate.convertAndSend(topic, email);
            System.out.println(" [x] Sent '" + email + email.toString()+"--topic");
        }

    }
}
